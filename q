[1mdiff --git a/app/Http/Controllers/Admin/Admins/AdminController.php b/app/Http/Controllers/Admin/Admins/AdminController.php[m
[1mindex 54ceebf..eb23774 100644[m
[1m--- a/app/Http/Controllers/Admin/Admins/AdminController.php[m
[1m+++ b/app/Http/Controllers/Admin/Admins/AdminController.php[m
[36m@@ -86,6 +86,10 @@[m [mclass AdminController extends Controller[m
         $admin = Admin::find($id);[m
         $admin->update($vars);[m
 [m
[32m+[m[32m        $activity = Activity::all();[m
[32m+[m
[32m+[m[32m        dd($activity);[m
[32m+[m
         return redirect()->route('admin.admins.index');[m
     }[m
 [m
[1mdiff --git a/app/Models/Admin.php b/app/Models/Admin.php[m
[1mindex 86dcf43..c4334b7 100644[m
[1m--- a/app/Models/Admin.php[m
[1m+++ b/app/Models/Admin.php[m
[36m@@ -6,11 +6,12 @@[m [muse Illuminate\Contracts\Auth\MustVerifyEmail;[m
 use Illuminate\Foundation\Auth\User as Authenticatable;[m
 use Illuminate\Notifications\Notifiable;[m
 use Illuminate\Database\Eloquent\SoftDeletes;[m
[32m+[m[32muse Spatie\Activitylog\Traits\LogsActivity;[m
 [m
 [m
 class Admin extends Authenticatable[m
 {[m
[31m-    use Notifiable, SoftDeletes;[m
[32m+[m[32m    use Notifiable, SoftDeletes, LogsActivity;[m
 [m
     const STATUS_INACTIVE = 0;[m
     const STATUS_ACTIVE = 1;[m
[36m@@ -21,9 +22,11 @@[m [mclass Admin extends Authenticatable[m
      * @var array[m
      */[m
     protected $fillable = [[m
[31m-        'last_name', 'first_name', 'email', 'email_verified_at', 'password', 'statuts'[m
[32m+[m[32m        'last_name', 'first_name', 'email', 'email_verified_at', 'password', 'status'[m
     ];[m
 [m
[32m+[m[32m    protected static $logAttributes = ['last_name', 'first_name', 'email', 'password', 'status'];[m
[32m+[m
     /**[m
      * The attributes that should be hidden for arrays.[m
      *[m
[1mdiff --git a/composer.json b/composer.json[m
[1mindex bfa54bd..36f4469 100644[m
[1m--- a/composer.json[m
[1m+++ b/composer.json[m
[36m@@ -12,7 +12,8 @@[m
         "fideloper/proxy": "^4.0",[m
         "laravel/framework": "^6.2",[m
         "laravel/tinker": "^1.0",[m
[31m-        "laravel/ui": "^1.1"[m
[32m+[m[32m        "laravel/ui": "^1.1",[m
[32m+[m[32m        "spatie/laravel-activitylog": "^3.9"[m
     },[m
     "require-dev": {[m
         "facade/ignition": "^1.4",[m
[1mdiff --git a/composer.lock b/composer.lock[m
[1mindex 1fa184c..0d2b219 100644[m
[1m--- a/composer.lock[m
[1m+++ b/composer.lock[m
[36m@@ -4,8 +4,58 @@[m
         "Read more about it at https://getcomposer.org/doc/01-basic-usage.md#installing-dependencies",[m
         "This file is @generated automatically"[m
     ],[m
[31m-    "content-hash": "1b9e73ad57a9bbfb35d3c7cc0d815d97",[m
[32m+[m[32m    "content-hash": "f840fd22fead77b15eb8b056c8fb70e4",[m
     "packages": [[m
[32m+[m[32m        {[m
[32m+[m[32m            "name": "anahkiasen/underscore-php",[m
[32m+[m[32m            "version": "2.0.0",[m
[32m+[m[32m            "source": {[m
[32m+[m[32m                "type": "git",[m
[32m+[m[32m                "url": "https://github.com/Anahkiasen/underscore-php.git",[m
[32m+[m[32m                "reference": "48f97b295c82d99c1fe10d8b0684c43f051b5580"[m
[32m+[m[32m            },[m
[32m+[m[32m            "dist": {[m
[32m+[m[32m                "type": "zip",[m
[32m+[m[32m                "url": "https://api.github.com/repos/Anahkiasen/underscore-php/zipball/48f97b295c82d99c1fe10d8b0684c43f051b5580",[m
[32m+[m[32m                "reference": "48f97b295c82d99c1fe10d8b0684c43f051b5580",[m
[32m+[m[32m                "shasum": ""[m
[32m+[m[32m            },[m
[32m+[m[32m            "require": {[m
[32m+[m[32m                "doctrine/inflector": "^1.0",[m
[32m+[m[32m                "patchwork/utf8": "^1.2",[m
[32m+[m[32m                "php": ">=5.4.0"[m
[32m+[m[32m            },[m
[32m+[m[32m            "require-dev": {[m
[32m+[m[32m                "fabpot/php-cs-fixer": "2.0.*@dev",[m
[32m+[m[32m                "phpunit/phpunit": "^4.6"[m
[32m+[m[32m            },[m
[32m+[m[32m            "type": "library",[m
[32m+[m[32m            "autoload": {[m
[32m+[m[32m                "psr-4": {[m
[32m+[m[32m                    "Underscore\\": [[m
[32m+[m[32m                        "src",[m
[32m+[m[32m                        "tests"[m
[32m+[m[32m                    ][m
[32m+[m[32m                }[m
[32m+[m[32m            },[m
[32m+[m[32m            "notification-url": "https://packagist.org/downloads/",[m
[32m+[m[32m            "license": [[m
[32m+[m[32m                "MIT"[m
[32m+[m[32m            ],[m
[32m+[m[32m            "authors": [[m
[32m+[m[32m                {[m
[32m+[m[32m                    "name": "Maxime Fabre",[m
[32m+[m[32m                    "email": "ehtnam6@gmail.com"[m
[32m+[m[32m                }[m
[32m+[m[32m            ],[m
[32m+[m[32m            "description": "A redacted port of Underscore.js for PHP",[m
[32m+[m[32m            "keywords": [[m
[32m+[m[32m                "internals",[m
[32m+[m[32m                "laravel",[m
[32m+[m[32m                "toolkit"[m
[32m+[m[32m            ],[m
[32m+[m[32m            "time": "2015-05-16T19:24:58+00:00"[m
[32m+[m[32m        },[m
         {[m
             "name": "dnoegel/php-xdg-base-dir",[m
             "version": "0.1",[m
[36m@@ -1123,6 +1173,65 @@[m
             ],[m
             "time": "2018-07-02T15:55:56+00:00"[m
         },[m
[32m+[m[32m        {[m
[32m+[m[32m            "name": "patchwork/utf8",[m
[32m+[m[32m            "version": "v1.3.1",[m
[32m+[m[32m            "source": {[m
[32m+[m[32m                "type": "git",[m
[32m+[m[32m                "url": "https://github.com/tchwork/utf8.git",[m
[32m+[m[32m                "reference": "30ec6451aec7d2536f0af8fe535f70c764f2c47a"[m
[32m+[m[32m            },[m
[32m+[m[32m            "dist": {[m
[32m+[m[32m                "type": "zip",[m
[32m+[m[32m                "url": "https://api.github.com/repos/tchwork/utf8/zipball/30ec6451aec7d2536f0af8fe535f70c764f2c47a",[m
[32m+[m[32m                "reference": "30ec6451aec7d2536f0af8fe535f70c764f2c47a",[m
[32m+[m[32m                "shasum": ""[m
[32m+[m[32m            },[m
[32m+[m[32m            "require": {[m
[32m+[m[32m                "lib-pcre": ">=7.3",[m
[32m+[m[32m                "php": ">=5.3.0"[m
[32m+[m[32m            },[m
[32m+[m[32m            "suggest": {[m
[32m+[m[32m                "ext-iconv": "Use iconv for best performance",[m
[32m+[m[32m                "ext-intl": "Use Intl for best performance",[m
[32m+[m[32m                "ext-mbstring": "Use Mbstring for best performance",[m
[32m+[m[32m                "ext-wfio": "Use WFIO for UTF-8 filesystem access on Windows"[m
[32m+[m[32m            },[m
[32m+[m[32m            "type": "library",[m
[32m+[m[32m            "extra": {[m
[32m+[m[32m                "branch-alias": {[m
[32m+[m[32m                    "dev-master": "1.3-dev"[m
[32m+[m[32m                }[m
[32m+[m[32m            },[m
[32m+[m[32m            "autoload": {[m
[32m+[m[32m                "psr-4": {[m
[32m+[m[32m                    "Patchwork\\": "src/Patchwork/"[m
[32m+[m[32m                },[m
[32m+[m[32m                "classmap": [[m
[32m+[m[32m                    "src/Normalizer.php"[m
[32m+[m[32m                ][m
[32m+[m[32m            },[m
[32m+[m[32m            "notification-url": "https://packagist.org/downloads/",[m
[32m+[m[32m            "license": [[m
[32m+[m[32m                "(Apache-2.0 or GPL-2.0)"[m
[32m+[m[32m            ],[m
[32m+[m[32m            "authors": [[m
[32m+[m[32m                {[m
[32m+[m[32m                    "name": "Nicolas Grekas",[m
[32m+[m[32m                    "email": "p@tchwork.com"[m
[32m+[m[32m                }[m
[32m+[m[32m            ],[m
[32m+[m[32m            "description": "Portable and performant UTF-8, Unicode and Grapheme Clusters for PHP",[m
[32m+[m[32m            "homepage": "https://github.com/tchwork/utf8",[m
[32m+[m[32m            "keywords": [[m
[32m+[m[32m                "grapheme",[m
[32m+[m[32m                "i18n",[m
[32m+[m[32m                "unicode",[m
[32m+[m[32m                "utf-8",[m
[32m+[m[32m                "utf8"[m
[32m+[m[32m            ],[m
[32m+[m[32m            "time": "2016-05-18T13:57:10+00:00"[m
[32m+[m[32m        },[m
         {[m
             "name": "phpoption/phpoption",[m
             "version": "1.5.0",[m
[36m@@ -1473,6 +1582,137 @@[m
             ],[m
             "time": "2018-07-19T23:38:55+00:00"[m
         },[m
[32m+[m[32m        {[m
[32m+[m[32m            "name": "spatie/laravel-activitylog",[m
[32m+[m[32m            "version": "3.9.1",[m
[32m+[m[32m            "source": {[m
[32m+[m[32m                "type": "git",[m
[32m+[m[32m                "url": "https://github.com/spatie/laravel-activitylog.git",[m
[32m+[m[32m                "reference": "659738573f8607191afbd2b794db8669a5b20951"[m
[32m+[m[32m            },[m
[32m+[m[32m            "dist": {[m
[32m+[m[32m                "type": "zip",[m
[32m+[m[32m                "url": "https://api.github.com/repos/spatie/laravel-activitylog/zipball/659738573f8607191afbd2b794db8669a5b20951",[m
[32m+[m[32m                "reference": "659738573f8607191afbd2b794db8669a5b20951",[m
[32m+[m[32m                "shasum": ""[m
[32m+[m[32m            },[m
[32m+[m[32m            "require": {[m
[32m+[m[32m                "illuminate/config": "5.8.*|^6.0",[m
[32m+[m[32m                "illuminate/database": "5.8.*|^6.0",[m
[32m+[m[32m                "illuminate/support": "5.8.*|^6.0",[m
[32m+[m[32m                "php": "^7.2",[m
[32m+[m[32m                "spatie/string": "^2.1"[m
[32m+[m[32m            },[m
[32m+[m[32m            "require-dev": {[m
[32m+[m[32m                "ext-json": "*",[m
[32m+[m[32m                "orchestra/testbench": "3.8.*|^4.0",[m
[32m+[m[32m                "phpunit/phpunit": "^7.5|^8.0",[m
[32m+[m[32m                "scrutinizer/ocular": "^1.5"[m
[32m+[m[32m            },[m
[32m+[m[32m            "type": "library",[m
[32m+[m[32m            "extra": {[m
[32m+[m[32m                "laravel": {[m
[32m+[m[32m                    "providers": [[m
[32m+[m[32m                        "Spatie\\Activitylog\\ActivitylogServiceProvider"[m
[32m+[m[32m                    ][m
[32m+[m[32m                }[m
[32m+[m[32m            },[m
[32m+[m[32m            "autoload": {[m
[32m+[m[32m                "psr-4": {[m
[32m+[m[32m                    "Spatie\\Activitylog\\": "src"[m
[32m+[m[32m                },[m
[32m+[m[32m                "files": [[m
[32m+[m[32m                    "src/helpers.php"[m
[32m+[m[32m                ][m
[32m+[m[32m            },[m
[32m+[m[32m            "notification-url": "https://packagist.org/downloads/",[m
[32m+[m[32m            "license": [[m
[32m+[m[32m                "MIT"[m
[32m+[m[32m            ],[m
[32m+[m[32m            "authors": [[m
[32m+[m[32m                {[m
[32m+[m[32m                    "name": "Freek Van der Herten",[m
[32m+[m[32m                    "email": "freek@spatie.be",[m
[32m+[m[32m                    "homepage": "https://spatie.be",[m
[32m+[m[32m                    "role": "Developer"[m
[32m+[m[32m                },[m
[32m+[m[32m                {[m
[32m+[m[32m                    "name": "Sebastian De Deyne",[m
[32m+[m[32m                    "email": "sebastian@spatie.be",[m
[32m+[m[32m                    "homepage": "https://spatie.be",[m
[32m+[m[32m                    "role": "Developer"[m
[32m+[m[32m                },[m
[32m+[m[32m                {[m
[32m+[m[32m                    "name": "Tom Witkowski",[m
[32m+[m[32m                    "email": "dev.gummibeer@gmail.com",[m
[32m+[m[32m                    "homepage": "https://gummibeer.de",[m
[32m+[m[32m                    "role": "Developer"[m
[32m+[m[32m                }[m
[32m+[m[32m            ],[m
[32m+[m[32m            "description": "A very simple activity logger to monitor the users of your website or application",[m
[32m+[m[32m            "homepage": "https://github.com/spatie/activitylog",[m
[32m+[m[32m            "keywords": [[m
[32m+[m[32m                "activity",[m
[32m+[m[32m                "laravel",[m
[32m+[m[32m                "log",[m
[32m+[m[32m                "spatie",[m
[32m+[m[32m                "user"[m
[32m+[m[32m            ],[m
[32m+[m[32m            "time": "2019-10-15T07:39:07+00:00"[m
[32m+[m[32m        },[m
[32m+[m[32m        {[m
[32m+[m[32m            "name": "spatie/string",[m
[32m+[m[32m            "version": "2.2.2",[m
[32m+[m[32m            "source": {[m
[32m+[m[32m                "type": "git",[m
[32m+[m[32m                "url": "https://github.com/spatie/string.git",[m
[32m+[m[32m                "reference": "28607b9925b4f0499d48570553ca419c6298e26b"[m
[32m+[m[32m            },[m
[32m+[m[32m            "dist": {[m
[32m+[m[32m                "type": "zip",[m
[32m+[m[32m                "url": "https://api.github.com/repos/spatie/string/zipball/28607b9925b4f0499d48570553ca419c6298e26b",[m
[32m+[m[32m                "reference": "28607b9925b4f0499d48570553ca419c6298e26b",[m
[32m+[m[32m                "shasum": ""[m
[32m+[m[32m            },[m
[32m+[m[32m            "require": {[m
[32m+[m[32m                "anahkiasen/underscore-php": "^2.0",[m
[32m+[m[32m                "php": ">=5.6.0"[m
[32m+[m[32m            },[m
[32m+[m[32m            "require-dev": {[m
[32m+[m[32m                "phpunit/phpunit": "5.*",[m
[32m+[m[32m                "scrutinizer/ocular": "~1.1"[m
[32m+[m[32m            },[m
[32m+[m[32m            "type": "library",[m
[32m+[m[32m            "autoload": {[m
[32m+[m[32m                "files": [[m
[32m+[m[32m                    "src/string_functions.php"[m
[32m+[m[32m                ],[m
[32m+[m[32m                "psr-4": {[m
[32m+[m[32m                    "Spatie\\String\\": "src"[m
[32m+[m[32m                }[m
[32m+[m[32m            },[m
[32m+[m[32m            "notification-url": "https://packagist.org/downloads/",[m
[32m+[m[32m            "license": [[m
[32m+[m[32m                "MIT"[m
[32m+[m[32m            ],[m
[32m+[m[32m            "authors": [[m
[32m+[m[32m                {[m
[32m+[m[32m                    "name": "Freek Van der Herten",[m
[32m+[m[32m                    "email": "freek@spatie.be",[m
[32m+[m[32m                    "homepage": "https://murze.be",[m
[32m+[m[32m                    "role": "Developer"[m
[32m+[m[32m                }[m
[32m+[m[32m            ],[m
[32m+[m[32m            "description": "String handling evolved",[m
[32m+[m[32m            "homepage": "https://github.com/spatie/string",[m
[32m+[m[32m            "keywords": [[m
[32m+[m[32m                "handling",[m
[32m+[m[32m                "handy",[m
[32m+[m[32m                "spatie",[m
[32m+[m[32m                "string"[m
[32m+[m[32m            ],[m
[32m+[m[32m            "time": "2017-11-08T14:28:57+00:00"[m
[32m+[m[32m        },[m
         {[m
             "name": "swiftmailer/swiftmailer",[m
             "version": "v6.2.1",[m
[1mdiff --git a/resources/views/admin/partials/sidebar.blade.php b/resources/views/admin/partials/sidebar.blade.php[m
[1mindex ed59916..e21961d 100644[m
[1m--- a/resources/views/admin/partials/sidebar.blade.php[m
[1m+++ b/resources/views/admin/partials/sidebar.blade.php[m
[36m@@ -117,7 +117,7 @@[m
 [m
                 <!-- Activity Logs -->[m
                 <li class="nav-item">[m
[31m-                    <a href="#" class="nav-link active">[m
[32m+[m[32m                    <a href="{{ route('admin.activity_logs') }}" class="nav-link active">[m
                         <i class="nav-icon fas fa-list"></i>[m
                         <p>Activity Logs</p>[m
                     </a>[m
[1mdiff --git a/routes/web.php b/routes/web.php[m
[1mindex 088e3d2..63831ba 100644[m
[1m--- a/routes/web.php[m
[1m+++ b/routes/web.php[m
[36m@@ -19,9 +19,8 @@[m [mAuth::routes();[m
 [m
 Route::get('/home', 'HomeController@index')->name('home');[m
 [m
[31m-[m
[31m-[m
 Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function() {[m
[32m+[m
 	Route::namespace('Auth')->middleware('guest:admin')->group(function() {[m
 		Route::get('login', 'LoginController@showLoginForm')->name('login');[m
 		Route::post('login', 'LoginController@login')->name('login');[m
[36m@@ -41,6 +40,10 @@[m [mRoute::namespace('Admin')->prefix('admin')->name('admin.')->group(function() {[m
 			Route::get('admin/{id}/edit', 'AdminController@edit')->name('admins.edit');[m
 			Route::post('admin/{id}', 'AdminController@update')->name('admins.update');[m
 			Route::delete('admin/{id}', 'AdminController@destroy')->name('admins.destroy');[m
[32m+[m
[32m+[m			[32mRoute::get('activity_logs', function() {[m
[32m+[m				[32mreturn view('admin.pages.activity_logs.index');[m
[32m+[m			[32m})->name('activity_logs');[m
 		});[m
 [m
 		Route::namespace('Users')->group(function() {[m
