@extends('admin.layouts.auth')

@section('content')
	<div class="card card-info w-75 mx-auto">
      	<div class="card-header">
            <h1 class="card-title">Log in</h1>
      	</div>
      	<!-- /.card-header -->

     		<!-- form start -->
            <form method="POST" action="{{ route('admin.login') }}" class="form-horizontal">
            	@csrf
            	
                <div class="card-body">
                  	<div class="form-group row">
                    	<label for="email" class="col-sm-2 col-form-label">Email</label>
                    	<div class="col-sm-10">
                      		<input type="email" name="email" class="form-control" id="email" placeholder="Email">
                    	</div>
                  	</div>

                  	<div class="form-group row">
                    	<label for="password" class="col-sm-2 col-form-label">Password</label>
                    	<div class="col-sm-10">
                      		<input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    	</div>
                  	</div>

                  	<div class="form-group row">
                    	<div class="offset-sm-2 col-sm-10">
	                      	<a href="#">I forgot my password</a>
                    	</div>
                  	</div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer text-center">
                  	<button type="submit" class="btn btn-info">Sign in</button>
                </div>
                <!-- /.card-footer -->

          	</form>
    </div>
    <!-- /.card -->
@endsection