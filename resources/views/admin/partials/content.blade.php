<div class="content">
	<div class="content-header">
      	<div class="container-fluid">
        	@yield('breadcrumb')
      	</div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="container-fluid">
        @yield('content')
    </div>
</div>