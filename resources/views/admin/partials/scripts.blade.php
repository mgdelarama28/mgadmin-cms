<!-- jQuery -->
<script src="{{ asset('js/jquery.min.js') }}"></script>

<!-- Bootstrap 4 -->
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

<!-- AdminLTE App -->
<script src="{{ asset('js/adminlte.min.js') }}"></script>

<!-- Data Table -->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

<!-- Data Table Call -->
<script>
	$(document).ready( function () {
	    $('#dataTable').DataTable();
	} );	
</script>

<!-- Sweet Alert 2 -->
{{-- <script src="sweetalert2.all.min.js"></script> --}}

<script>
    $(".deleteItem").on("submit", function(){
        return confirm("Are you sure?");
    });
</script>