<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        @auth('admin')
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.logout') }}">
                    <img class="img-circle mr-1" src="{{ Auth::user()->renderDisplayImage() }}" width="30px" height="30px" alt="Me">
                    {{ Auth::user()->renderFullName() }} <span class="font-weight-bold">(Logout)</span>
                </a>
            </li>
        @endauth
    </ul>
</nav>