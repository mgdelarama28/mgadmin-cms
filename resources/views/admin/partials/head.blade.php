<head>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta http-equiv="x-ua-compatible" content="ie=edge">
  	<title>{{ config('app.name') }}</title>
  	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
  	<!-- Data Table CSS -->
  	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
</head>

