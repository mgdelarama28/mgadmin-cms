@extends('admin.layouts.master')

@section('breadcrumb')
	<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark font-weight-bold">Users Index</h1>
			</div><!-- /.col -->

			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">Users</a></li>
						<li class="breadcrumb-item active">Index</li>
				</ol>
			</div><!-- /.col -->
	</div><!-- /.row -->
	<hr>
@endsection

@section('content')
	<div class="content">
		<div class="container-fluid">
			
			<h3 class="mb-3">List of Users</h3>

			<table id="dataTable" class="table table-striped table-bordered">
				<thead>
					<tr class="text-center">
						<th>ID</th>
						<th>Avatar</th>
						<th>Last Name</th>
						<th>First Name</th>
						<th>Email</th>
						<th>Action</th>
					</tr>
				</thead>

				<tbody>
					@foreach ($users as $user)
						<tr class="text-center">
							<td>{{ $user->renderId() }}</td>
							<td><img src="{{ $user->renderDisplayImage() }}" alt="Admin Image" width="50px" height="50px"></td>
							<td>{{ $user->renderLastName() }}</td>
							<td>{{ $user->renderFirstName() }}</td>
							<td>{{ $user->renderEmail() }}</td>
							<td class="d-flex justify-content-center">
								<a href="{{ route('admin.users.show', $user->id) }}" class="btn btn-primary mx-1">
									<i class="fas fa-eye"></i>
								</a>
								<a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-primary mx-1">
									<i class="fas fa-edit"></i>
								</a>

								<form class="deleteItem mx-1" action="{{ route('admin.users.destroy', $user->id) }}" method="POST">
									@csrf
									{{ method_field('DELETE') }}

									<button class="btn btn-danger"><i class="fas fa-trash"></i></button>
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection