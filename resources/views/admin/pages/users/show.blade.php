@extends('admin.layouts.master')

@section('breadcrumb')
	<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark font-weight-bold">Show User</h1>
			</div><!-- /.col -->

			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">Users</a></li>
						<li class="breadcrumb-item active">Show</li>
				</ol>
			</div><!-- /.col -->
	</div><!-- /.row -->
	<hr>
@endsection

@section('content')
	<div class="content">
		<div class="container-fluid">
			<div class="d-flex mb-3">
				<a href="{{ route('admin.users.show', $user->id) }}" class="btn btn-primary mr-1">
					<i class="fas fa-edit"></i>
					Edit
				</a>
		        <div>
		            <form class="deleteItem" action="{{ route('admin.users.destroy', $user->id) }}" method="POST">
		            	@csrf
		            	{{ method_field('DELETE') }}

		            	<button class="btn btn-danger"><i class="fas fa-trash mr-1"></i>Delete</button>
		            </form>
		        </div>
		    </div>

		    <div class="row">
		        <div class="col-md-12">
		            <div class="box">
		                <div class="box-header mb-3">
		                    <h3 class="box-title">User Details</h3>
		                </div>

		                <div class="box-body">
		                    <div class="row">
		                    	<div class="form-group col-md-6">
		                            <label for="email">ID</label>
		                            <p>{{ $user->renderId() }}</p>
		                        </div>

		                        <div class="form-group col-md-6">
		                            <label for="email">Email</label>
		                            <p>{{ $user->renderEmail() }}</p>
		                        </div>

		                        <div class="form-group col-md-6">
		                            <label for="lastName">Last Name</label>
		                            <p>{{ $user->renderLastName() }}</p>
		                        </div>

		                        <div class="form-group col-md-6">
		                            <label for="firstName">First Name</label>
		                            <p>{{ $user->renderFirstName() }}</p>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
@endsection