@extends('admin.layouts.master')

@section('breadcrumb')
	<div class="row mb-2">
  		<div class="col-sm-6">
    		<h1 class="m-0 text-dark font-weight-bold">Dashboard</h1>
  		</div><!-- /.col -->

  		<div class="col-sm-6">
    		<ol class="breadcrumb float-sm-right">
      			<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
    		</ol>
  		</div><!-- /.col -->
	</div><!-- /.row -->
@endsection

@section('content')
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3 col-6">
		            <!-- small box -->
		            <div class="small-box bg-warning">
		              	<div class="inner">
		                	<h3>{{ $adminCount }}</h3>
			                <p>Admin Count</p>
		             	</div>

		              	<div class="icon">
		                	<i class="ion ion-person-add"></i>
		              	</div>
		              	<a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		            </div>
	          	</div>
		          <!-- ./col -->

				<div class="col-lg-3 col-6">
		            <!-- small box -->
		            <div class="small-box bg-info">
		              	<div class="inner">
		                	<h3>{{ $userCount }}</h3>
		                	<p>User Registrations</p>
		              	</div>

		              	<div class="icon">
		                	<i class="ion ion-bag"></i>
		              	</div>
		              	<a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		            </div>
		         </div>
			</div>
		</div>
	</div>
@endsection