@extends('admin.layouts.master')

@section('breadcrumb')
	<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark font-weight-bold">Actvity Logs</h1>
			</div><!-- /.col -->

			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ route('admin.activity_logs.index') }}">Activity Logs</a></li>
						<li class="breadcrumb-item active">Index</li>
				</ol>
			</div><!-- /.col -->
	</div><!-- /.row -->
	<hr>
@endsection

@section('content')
	<div class="content">
		<div class="container-fluid">
			
			<h3 class="mb-3">List of Activities</h3>

			<table id="dataTable" class="table table-striped table-bordered">
				<thead>
					<tr class="text-center">
						<th>ID</th>
						<th>Message</th>
						<th>Date</th>
					</tr>
				</thead>

				<tbody>
					@foreach ($activityLogs as $activityLog)
						<tr class="text-center">
							<td>{{ $activityLog->id }}</td>
							<td>{{ $activityLog->log_name }} {{ $activityLog->description }} Admin #{{ $activityLog->subject_id }}</td>
							<td>{{ \Carbon\Carbon::parse($activityLog->created_at)->format('d/m/Y h:m:s a') }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection