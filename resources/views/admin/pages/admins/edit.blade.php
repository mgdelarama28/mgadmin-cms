@extends('admin.layouts.master')

@section('breadcrumb')
	<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark font-weight-bold">Edit Admin</h1>
			</div><!-- /.col -->

			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ route('admin.admins.index') }}">Admins</a></li>
						<li class="breadcrumb-item active">Edit</li>
				</ol>
			</div><!-- /.col -->
	</div><!-- /.row -->
	<hr>
@endsection

@section('content')
	<div class="content">
		<div class="container-fluid">
			<h3 class="mb-3">Admin Details</h3>					

			<form action="{{ route('admin.admins.update', $admin->id) }}" method="POST">
				@csrf
				<div class="row">
					<div class="form-group col-md-6">
						<label for="last_name">Last Name</label>
						<input type="text" class="form-control" name="last_name" value="{{ $admin->renderLastName() }}" required>
					</div>

					<div class="form-group col-md-6">
						<label for="first_name">First Name</label>
						<input type="text" class="form-control" name="first_name" value="{{ $admin->renderFirstName() }}" required>
					</div>	

					<div class="form-group col-md-6">
						<label for="email">Email</label>
						<input type="email" name="email" class="form-control" value="{{ $admin->renderEmail() }}" required>
					</div>
				</div>
				
				<button class="btn btn-primary">Update</button>
			</form>		
		</div>
	</div>
@endsection