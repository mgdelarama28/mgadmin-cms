@extends('admin.layouts.master')

@section('breadcrumb')
	<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark font-weight-bold">Admins Index</h1>
			</div><!-- /.col -->

			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ route('admin.admins.index') }}">Admins</a></li>
						<li class="breadcrumb-item active">Index</li>
				</ol>
			</div><!-- /.col -->
	</div><!-- /.row -->
	<hr>
@endsection

@section('content')
	<div class="content">
		<div class="container-fluid">
			
			<h3 class="mb-3">List of Admins</h3>

			<table id="dataTable" class="table table-striped table-bordered">
				<thead>
					<tr class="text-center">
						<th>ID</th>
						<th>Avatar</th>
						<th>Last Name</th>
						<th>First Name</th>
						<th>Email</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>

				<tbody>
					@foreach ($admins as $admin)
						<tr class="text-center">
							<td>{{ $admin->renderId() }}</td>
							<td><img src="{{ $admin->renderDisplayImage() }}" alt="Admin Image" width="50px" height="50px"></td>
							<td>{{ $admin->renderLastName() }}</td>
							<td>{{ $admin->renderFirstName() }}</td>
							<td>{{ $admin->renderEmail() }}</td>
							<td>{{ $admin->renderStatus() }}</td>
							<td class="d-flex justify-content-center">
								<a href="{{ route('admin.admins.show', $admin->id) }}" class="btn btn-primary mx-1">
									<i class="fas fa-eye"></i>
								</a>
								<a href="{{ route('admin.admins.edit', $admin->id) }}" class="btn btn-primary mx-1">
									<i class="fas fa-edit"></i>
								</a>

								<form class="deleteItem mx-1" action="{{ route('admin.admins.destroy', $admin->id) }}" method="POST">
									@csrf
									{{ method_field('DELETE') }}

									<button class="btn btn-danger"><i class="fas fa-trash"></i></button>
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection