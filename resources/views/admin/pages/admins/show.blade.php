@extends('admin.layouts.master')

@section('breadcrumb')
	<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark font-weight-bold">Show Admin</h1>
			</div><!-- /.col -->

			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ route('admin.admins.index') }}">Admins</a></li>
						<li class="breadcrumb-item active">Show</li>
				</ol>
			</div><!-- /.col -->
	</div><!-- /.row -->
	<hr>
@endsection

@section('content')
	<div class="content">
		<div class="container-fluid">
			<div class="d-flex mb-3">
				<a href="{{ route('admin.admins.edit', $admin->id) }}" class="btn btn-primary mr-1">
					<i class="fas fa-edit"></i>
					Edit
				</a>	
				
		        <div>
		            <form class="deleteItem" action="{{ route('admin.admins.destroy', $admin->id) }}" method="POST">
		            	@csrf
		            	{{ method_field('DELETE') }}

		            	<button class="btn btn-danger"><i class="fas fa-trash mr-1"></i>Delete</button>
		            </form>
		        </div>
		    </div>

		    <div class="row">
		        <div class="col-md-12">
		            <div class="box">
		                <div class="box-header mb-3">
		                    <h3 class="box-title">Admin Details</h3>
		                </div>

		                <div class="box-body">
		                    <div class="row">
		                    	<div class="form-group col-md-6">
		                            <label for="email">ID</label>
		                            <p>{{ $admin->renderId() }}</p>
		                        </div>

		                        <div class="form-group col-md-6">
		                            <label for="email">Email</label>
		                            <p>{{ $admin->renderEmail() }}</p>
		                        </div>

		                        <div class="form-group col-md-6">
		                            <label for="lastName">Last Name</label>
		                            <p>{{ $admin->renderLastName() }}</p>
		                        </div>

		                        <div class="form-group col-md-6">
		                            <label for="firstName">First Name</label>
		                            <p>{{ $admin->renderFirstName() }}</p>
		                        </div>

		                        <div class="form-group col-md-6">
		                            <label for="status">Status</label>
		                            <p>{{ $admin->renderStatus() }}</p>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
@endsection