<!DOCTYPE html>
<html lang="en">
<!-- Head -->
@include('admin.partials.head')

<body class="hold-transition sidebar-mini">
    <div id="app" class="wrapper">

        <!-- Navbar -->
        @include('admin.partials.navbar')

        <!-- Main Sidebar Container -->
        @include('admin.partials.sidebar')

        <div class="content-wrapper">
            <!-- Content -->
            @include('admin.partials.content')
        </div>

        <!-- Footer -->
        @include('admin.partials.footer')

    </div>

    <!-- Scripts -->
    @include('admin.partials.scripts')
</body>
</html>
