<!DOCTYPE html>
<html lang="en">
<head>
	@include('admin.partials.head')
</head>
<body>
	<div id="app" class="container mt-5">
		<h1 class="text-center">{{ config('app.name') }} Admin Panel</h1>
		@yield('content')
	</div>
</body>
</html>