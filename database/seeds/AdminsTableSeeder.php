<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminsTableSeeder extends Seeder
{
	protected $admins = [
		[
			'last_name' => 'App',
			'first_name' => 'Admin',
			'email' => 'admin@app.com',
			'password' => '$2y$10$aVip0jjPo1sOu5trdxWg/ezZJNGqfT/tnP0AaPPQ9i7UrGCh53fIO',
			'status' => 1,
		],

        [
            'last_name' => 'Dela Rama',
            'first_name' => 'Michael Geofrey',
            'email' => 'mgdelarama.28@gmail.com',
            'password' => '$2y$10$aVip0jjPo1sOu5trdxWg/ezZJNGqfT/tnP0AaPPQ9i7UrGCh53fIO',
            'status' => 1,
        ]
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->admins as $key => $admin) {
        	$vars = [
        		'last_name' => $admin['last_name'],
        		'first_name' => $admin['first_name'],
        		'email' => $admin['email'],
        		'password' => $admin['password'],
        		'status' => $admin['status'],
        	];

        	\DB::beginTransaction();

        		Admin::create($vars);

        	\DB::commit();
        }
    }
}
