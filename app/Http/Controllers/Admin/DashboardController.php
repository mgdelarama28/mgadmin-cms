<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index() {
    	$adminCount = Admin::count();
    	$userCount = User::count();

    	return view('admin.pages.dashboard', [
    		'adminCount' => $adminCount,
    		'userCount' => $userCount,
    	]);
    }
}
