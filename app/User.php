<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name', 'first_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /** Renders */
    public function renderId()
    {
        return $this->id;
    }

    public function renderLastName()
    {
        return $this->last_name;
    }

    public function renderFirstName()
    {
        return $this->first_name;
    }

    public function renderFullName()
    {
        return $this->first_name. ' ' .$this->last_name;
    }

    public function renderEmail()
    {
        return $this->email;
    }

    public function renderDisplayImage()
    {
        if ($this->image_path) {
            return '/storage/' .$this->image_path;
        }

        return '/storage/no-image.jpg';
    }
}
