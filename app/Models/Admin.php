<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;


class Admin extends Authenticatable
{
    use Notifiable, SoftDeletes, LogsActivity;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name', 'first_name', 'email', 'email_verified_at', 'password', 'status'
    ];

    protected static $logAttributes = ['last_name', 'first_name', 'email', 'password', 'status'];

    protected static $logName = 'Default';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /* Renders */
    public function renderId()
    {
        return $this->id;
    }
    
    public function renderLastName() 
    {
        return $this->last_name;
    }

    public function renderFirstName()
    {
        return $this->first_name;
    }

    public function renderFullName()
    {
        return $this->first_name. ' ' .$this->last_name;
    }

    public function renderEmail()
    {
        return $this->email;
    }

    public function renderStatus()
    {
        if ($this->status == 0) {
            return 'Inactive';
        }

        return 'Active';
    }

    public function renderDisplayImage() 
    {
        if ($this->image_path) {
            return '/storage/' . $this->image_path;
        }

        return '/storage/no-image.jpg';
    }
}
