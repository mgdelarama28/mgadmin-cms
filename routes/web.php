<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function() {

	Route::namespace('Auth')->middleware('guest:admin')->group(function() {
		Route::get('login', 'LoginController@showLoginForm')->name('login');
		Route::post('login', 'LoginController@login')->name('login');

	});

	Route::middleware('auth:admin')->group(function() {
		Route::get('', 'DashboardController@index')->name('dashboard');

		Route::namespace('Auth')->group(function() {
			Route::get('logout', 'LoginController@logout')->name('logout');
		});

		Route::namespace('Admins')->group(function() {
			Route::get('admin', 'AdminController@index')->name('admins.index');
			Route::get('admin/{id}', 'AdminController@show')->name('admins.show');
			Route::get('admin/{id}/edit', 'AdminController@edit')->name('admins.edit');
			Route::post('admin/{id}', 'AdminController@update')->name('admins.update');
			Route::delete('admin/{id}', 'AdminController@destroy')->name('admins.destroy');
		});

		Route::namespace('Users')->group(function() {
			Route::get('users', 'UserController@index')->name('users.index');
			Route::get('user/{id}', 'UserController@show')->name('users.show');
			Route::get('user/{id}/edit', 'UserController@edit')->name('users.edit');
			Route::post('user/{id}', 'UserController@update')->name('users.update');
			Route::delete('user/{id}', 'UserController@destroy')->name('users.destroy');
		});

		Route::namespace('ActivityLogs')->group(function() {
			Route::get('activity_logs', 'ActivityLogController@index')->name('activity_logs.index');
		});
	});


});
